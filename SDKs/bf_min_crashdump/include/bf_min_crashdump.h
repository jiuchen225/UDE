﻿#pragma once

#ifdef BF_MIN_CRASHDUMP_EXPORTS
#define BF_MIN_CRASHDUMP_API extern "C" __declspec(dllexport)
#else
#define BF_MIN_CRASHDUMP_API extern "C" __declspec(dllimport)
#endif



BF_MIN_CRASHDUMP_API
int bf_generate_mini_dump(void* data, const char *Message);

